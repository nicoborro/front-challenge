import styles from "../../styles/Container.module.css";
import { ResultSet } from "../../components/";

const defaultEndpoint = "http://localhost:3000/api/items?q=";

export async function getServerSideProps(context) {
  const { query } = context;

  const q = query.search;

  const res = await fetch(defaultEndpoint + q);
  const data = await res.json();
  return { props: { data } };
}

export default function Items({ data }) {
  return <div className={styles.container}>
    <ResultSet data={data} />
  </div>;
}
