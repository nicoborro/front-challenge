import styles from "../../../styles/Container.module.css";
import { Item } from "../../../components";

const defaultEndpoint = "http://localhost:3000/api/items/";

export async function getServerSideProps(context) {
  const { query } = context;

  let data = [];
  const res = await fetch(defaultEndpoint + query.id);
  data = await res.json();
  return { props: { data } };
}

export default function ProductDetails({ data }) {
  return (
    <div className={styles.container}>
      {data.id ? <Item item={data} /> : <div>Item not found</div>}
    </div>
  );
}
