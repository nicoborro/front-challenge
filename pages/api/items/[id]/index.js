import db from "../../../../db.json";

export default function handler(req, res) {
  const { id } = req.query;
  
  if (id) {
    const item = db.find((item) => item.id === +id);
    return res.status(200).json(item? item : {});
  }

  res.status(200).json();
}
