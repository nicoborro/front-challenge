import db from "../../../db.json";

export default function handler(req, res) {
  const { id, q } = req.query;

  //id available
  if (id) {
    const item = db.find((item) => item.id === +id);
    return res.status(200).json(item);
  }

  //keyword to search for
  if (q) {
    const results = db.filter((product) => {
      const { title, description } = product;
      return (
        title.toLowerCase().includes(q.toLowerCase()) ||
        description.toLowerCase().includes(q.toLowerCase())
      );
    });
    return res.status(200).json(results);
  }

  //if we don't have anything
  //res.status(200).json({status: 'Nothing found'});
  res.status(200).json(db);
}
