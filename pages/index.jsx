import { useState } from "react";
import styles from "../styles/Container.module.css";
import { Header, ResultSet } from "../components";

export default function Home() {
  const [data, setData] = useState("");

  return (
      <div className={styles.container}>
        <Header setData={setData} />
        <ResultSet data={data} />
      </div>
  );
}
