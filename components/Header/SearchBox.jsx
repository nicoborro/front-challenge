import { useState, useEffect } from "react";
import styles from "../../styles/Header.module.css";

const defaultEndpoint = "http://localhost:3000/api/items";

export default function SearchBox({ setData }) {
  const [query, setQuery] = useState("");

  useEffect(() => {
    fetch(defaultEndpoint + "?q=" + query)
      .then((response) => response.json())
      .then((data) => setData(data));
  }, [query, setData]);

  return (
    <div className={styles.searchBox}>
      <input
        value={query}
        placeholder="Search"
        className={styles.input}
        onChange={(event) => setQuery(event.target.value)}
      />
    </div>
  );
}
