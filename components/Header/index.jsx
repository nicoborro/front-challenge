import logo from "../../public/Assets/Qurable Logo.png";
import creatorimg from "../../public/Assets/bg-logo.png";
import styles from "../../styles/Header.module.css";
import SearchBox from "./SearchBox";
import Image from "next/image";

export default function Header({ setData }) {
  return (
    <div className={styles.header}>
      <div className={styles.logo}>
        <Image src={logo} alt={"Qurable"} />
      </div>
      <SearchBox setData={setData} />
      <Image src={creatorimg} alt="User" width="40%" height="40%" />
    </div>
  );
}
