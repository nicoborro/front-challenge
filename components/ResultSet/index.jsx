import { useState, useEffect } from "react";
import styles from "../../styles/Items.module.css";
import Item from "./Item";

export default function ResultSet({ data }) {
  return (
    <ul className={styles.items}>
      {!!data &&
        data.map((item) => {
          return <Item key={item.id} item={item} />;
        })}
    </ul>
  );
}
