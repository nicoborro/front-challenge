import { Router, useRouter } from "next/router";
import styles from "../../styles/Items.module.css";

export default function Item({ item }) {
  const { id, title, price, img } = item;
  const router = useRouter();

  return (
    <div key={id} className={styles.item} onClick={() => router.push("/items/" + item.id)} >
      <div className={styles.center}>
        <img className={styles.img} src={img} alt="Item" />
      </div>
      <div className={styles.text}>{price}</div>
      <div className={styles.text}>{title}</div>
    </div>
  );
}
